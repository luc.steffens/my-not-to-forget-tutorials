I had many issues on installing dualboot on the Asus Zenbook Deluxe 3 

Windows was fine, but after installing linux in the same partition, I lost all entries..

To avoid this, you need to update eufi using the efibootmgr.
You need to boot with another boot system to fix eufi in the installed linux (rEFInd.):

here are some resources

https://www.rodsbooks.com/efi-bootloaders/installation.html

    # efibootmgr -c -d /dev/sda -p 1 -l \\EFI\\newloader\\loadername.efi -L NewLoader
    

https://askubuntu.com/questions/626353/how-to-list-unmounted-partition-of-a-harddisk-and-mount-them

To address the listing of the unmounted partitions part, there are several ways - lsblk, fdisk, parted, blkid.

https://unix.stackexchange.com/questions/146784/is-grub-the-best-bootloading-solution-is-there-an-easier-alternative#146803

## For the Asus Zenbook Deluxe UX490UAR

The latest bios is a broken EFI (many, many retries later..). You need to move the grubx64.efi (> Sabayon default name) file to the root partion. There you can select.
The zenbook is fantastic, but really sad the linux-support isn't fully ok.

- The hybrid hibernate is just perfect, a lot of battery, super fast startup
- Sound and bluetooth doesn't work out of the box, still working on that
- wifi, screen brightness, keyboard lit, ... All just works for me (sabayon install - KDE)
- Super battery for an i7, much much much better then on windows..